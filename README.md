# Configurador de Ambiente

Projeto utilizado para configurar o ambiente utilizado durante as aulas do módulo de DevOps do Curso de Especialização Lato Sensu "Desenvolvimento de Software para a Web" da UFSCar.

# Conteúdo
1. [Configurador de Ambiente](#configurador-de-ambiente)
2. [Conteúdo](#conteúdo)
   1. [Instalação](#instalação)
   2. [Utilização](#utilização)
      1. [Docker](#docker)
      2. [Docker Compose](#docker-compose)
      3. [Kubectl](#kubectl)
      4. [Helm](#helm)
   3. [Contributing](#contributing)
   4. [License](#license)
   5. [Visuals](#visuals)
   6. [Installation](#installation)
   7. [Usage](#usage)
   8. [Support](#support)
   9. [Roadmap](#roadmap)
   10. [Contributing](#contributing-1)
   11. [Authors and acknowledgment](#authors-and-acknowledgment)
   12. [License](#license-1)
   13. [Project status](#project-status)

## Instalação

Baixe o projeto com o seguinte comando:

```shell
wget https://gitlab.com/latosensu/configurador-de-ambiente/-/archive/v1.0.0/configurador-de-ambiente-v1.0.0.zip
```

Descompacte usando o unzip:

```shell
unzip configurador-de-ambiente-v1.0.0.zip
```

Execute o comando a seguir no diretório `configurador-de-ambiente-v1.0.0` que foi criado com o comando anterior:


```shell
./configuar-ambiente-local.sh
```

Será necessário digitar a senha. Serão instaladas as seguintes dependências:

* Ansible - Responsável por automatizar a instalação das demais dependências.
* Git - Realiza o versionamento do código.
* Curl - Dependência utilizada para realizar a cópia de algumas outras dependências.
* Python 3 - Utilizado pelo Ansible.
* Docker - Responsável por inicializar e gerenciar *containers*.
* Docker Compose - Utilizado para definir e rodar aplicações com múltiplos *containers*.
* Kubectl - Programa de linha de comando utilizado para interagir com o Kubernetes.
* k0s - Distribuição do Kubernetes, permite a execução de um cluster local de Kubernetes.
* Skaffold - Utilizado para automatizar o fluxo de construção de imagem, submissão da mesma para um registry e implantação de aplicação.
* Helm - Ferramenta utilizada para definir e gerenciar pacotes (*charts*) que permitem a distribuição e execução de aplicações no Kubernetes.

Após a instalação, é importante reiniciar a máquina virtual.
## Utilização

Vamos utilizar as seguintes aplicações:
* Docker
* Docker Compose
* Kubectl
* Helm
* Skaffold

### Docker

O Docker pode ser acessado pelo terminal com o comando `docker`.

Para vermos as imagens baixadas podemos usar o comando:

```shell
docker images
```

A saída será semelhante a seguir:

```
REPOSITORY   TAG       IMAGE ID       CREATED       SIZE
registry     2         9c97225e83c8   10 days ago   24.2MB
```

Esse projeto já instala um *registry* do Docker, por isso a imagem será apresentada.

Para vermos os *containers* que estão em execução, use o comando:

```shell
docker ps
```

Inicialmente teremos uma saída semelhante a essa:

```
CONTAINER ID   IMAGE        COMMAND                  CREATED       STATUS       PORTS                                       NAMES
71cfdf09b3df   registry:2   "/entrypoint.sh /etc…"   3 hours ago   Up 3 hours   0.0.0.0:5005->5000/tcp, :::5005->5000/tcp   registry
```

Ela indica que existe um *container* com o nome registry ativo.

Para construirmos uma imagem com base em um arquivo *Dockerfile* podemos executar o comando a seguir:

```shell
docker build -t $NOME_DA_IMAGEM .
```

No qual $NOME_DA_IMAGEM deve ser substituído por um nome válido de imagem Docker. Os nomes de imagem do Docker devem seguir esse esquema:

```
[REPOSITORIO:]NOME_IMAGEM[:TAG]
```

REPOSITORIO é o endereço do registry onde a imagem deve ser publicada, NOME_IMAGEM é um nome arbitrário para a iamge e TAG representa a *tag* da image. Tanto o REPOSITORIO quanto a TAG são opcionais, e caso a tag não seja indicada, ela assume o valor padrão `latest`.

### Docker Compose

O Docker Compose pode ser acessado pelo terminal com o comando `docker-compose`.

Por exemplo, para iniciar um ambiente com base em um arquivo `docker-compose.yaml`, basta usar:

```shell
docker-compose up
```

Se desejar iniciar o ambiente em segundo plano, ou seja, sem travar o terminal, acrescente o parâmetro `-d` (*daemon*):

```shell
docker-compose up -d
```

Para parar o aambiente, basta usar o comando `down`: 

```shell
docker-compose down
```

### Kubectl

O kubectl pode ser executado no terminal com o comando `kubectl`. Alguns comandos interessantes são:

```
# Utilizado para mostrar os nós do cluster
kubectl get nodes
```

```
# Utilizado para mostrar os pods do cluster
kubectl get pods
```

```
# Utilizado para mostrar os pods do cluster
kubectl get services
```

```
# Utilizado para criar/alterar os recursos descritos no arquivo pod.yaml
kubectl apply -f pod.yaml
```
 
### Helm

O Helm pode ser executado no terminal com o comando `helm`. 

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)




## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
